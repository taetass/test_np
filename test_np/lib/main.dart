import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _showMore = false;

  String txt = "This aromatic type of biryani is popular in Pakistan and known for its spicy taste, fragrant rice, and delicate meat. This biryani is one of India's most popular types of biryani. It incorporates goat meat that is marinated and cooked along with the rice and is seasoned with coconut and saffron It is made with meat and basmati rice, vegetables, and various types of spices";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.network(
            'https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg',
            fit: BoxFit.cover,
          ),
          Positioned(
              top: 45,
              left: 20,
              right: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.7),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: const Padding(
                      padding: EdgeInsets.only(left: 8),
                      child: Icon(Icons.arrow_back_ios),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.7),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: const Icon(Icons.shopping_cart_outlined),
                  ),
                ],
              )),
          Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 200,
                    ),
                    Container(
                      height: 800,
                      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(22),
                            topLeft: Radius.circular(22),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffDCDCDC),
                              blurRadius: 4,
                              offset: Offset(0, 0),
                            ),
                          ],
                          color: Colors.white),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Fruit nutrition meal',
                                  style: Theme.of(context).textTheme.headlineMedium,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _showMore
                                    ? SizedBox(
                                        height: 200,
                                        child: Text(
                                          txt,
                                        ),
                                      )
                                    : Text(
                                        txt,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                      ),
                                const SizedBox(height: 8.0),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      _showMore = !_showMore;
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      const Text(
                                        'Expand',
                                        style: TextStyle(color: Color(0xFF89dad0)),
                                      ),
                                      Icon(
                                        _showMore ? Icons.arrow_drop_up : Icons.arrow_drop_down,color: const Color(0xFF89dad0),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  const SizedBox(
                    height: 180,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                          height: 50,
                          width: 50,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xFF89dad0),
                            boxShadow: const [
                              BoxShadow(
                                color: Color(0xffDCDCDC),
                                blurRadius: 4,
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          child: const Icon(
                            Icons.favorite,
                            color: Colors.white,
                          )),
                      const SizedBox(
                        width: 18,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: const Color(0xffDCDCDC).withOpacity(0.3),
        ),
        height: 100,
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: const EdgeInsets.all(18),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(22),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                    color: Color(0xffDCDCDC),
                    blurRadius: 4,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: const Row(
                children: [
                  Icon(
                    Icons.remove,
                    color: Colors.grey,
                  ),
                  SizedBox(width: 15),
                  Text(
                    "2",
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(width: 15),
                  Icon(
                    Icons.add,
                    color: Colors.grey,
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(18),
              child: const Text(
                "\$ 28 | Add to cart",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: const Color(0xFF89dad0),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0xffDCDCDC),
                    blurRadius: 4,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
